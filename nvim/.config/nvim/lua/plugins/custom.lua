return {
  {
    "saghen/blink.cmp",
    opts = { keymap = { preset = "super-tab" } },
  },
  {
    "ibhagwan/fzf-lua",
    opts = {
      grep = {
        hidden = true,
        rg_opts = "--column --line-number --no-heading --color=always --smart-case --max-columns=4096 --trim --glob !.git -e",
        rg_glob_fn = function(query)
          local regex, flags = query:match("^(.-)%s%-%-(.*)$")
          return (regex or query), flags
        end,
      },
    },
    keys = {
      { "<leader>gC", "<cmd>FzfLua git_bcommits<cr>", desc = "Commits (buffer)" },
      { "<leader>fa", LazyVim.pick("live_grep"), desc = "Grep (Root Dir)" },
      { "<leader>gs", false },
    },
  },
  {
    "nvim-lualine/lualine.nvim",
    opts = {
      sections = {
        lualine_y = {
          { "%3l/%-3L:%3v/%-3{virtcol('$')-1}", padding = { left = 1, right = 1 } },
        },
      },
    },
  },
  {
    "neovim/nvim-lspconfig",
    opts = function()
      local keys = require("lazyvim.plugins.lsp.keymaps").get()
      keys[#keys + 1] = { "gh", vim.lsp.buf.hover, desc = "Hover" }
    end,
  },
  {
    "folke/todo-comments.nvim",
    enabled = false,
  },
}
