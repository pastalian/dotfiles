vim.g.autoformat = false
vim.g.snacks_animate = false
vim.g.root_spec = { "cwd" }
LazyVim.terminal.setup("fish")
vim.opt.clipboard = "unnamedplus"
vim.opt.spelllang = { "en", "cjk" }
vim.opt.virtualedit = ""
