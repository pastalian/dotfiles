for p in ~/.cargo/bin ~/.local/bin ~/bin; do
    [[ -d $p/. ]] || continue
    [[ :$PATH: = *":$p:"* ]] || PATH=$p:$PATH
done

HISTFILE=$HOME/.cache/zsh/zhistory
HISTSIZE=1000
SAVEHIST=1000
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_save_no_dups
setopt inc_append_history

setopt ignore_eof
bindkey -e

autoload -Uz compinit; compinit -d $HOME/.cache/zsh/zcompdump
zstyle ':completion:*:default' menu select
# eval "$(dircolors -b)"
# zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

alias l='ls --color=auto' ll='l -lh' la='ll -A'
alias rm='rm -iv' cp='cp -iv'
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias vim='nvim'

export BROWSER=firefox
export EDITOR=/usr/bin/nvim
export LESSHISTFILE=-

export MOZC_CONFIGURATION_DIRECTORY=$HOME/.config/mozc
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
export DefaultIMModule=fcitx

export FZF_DEFAULT_COMMAND='fd --type f --hidden --exclude .git'
export QT_STYLE_OVERRIDE=adwaita

# fix java program issue with some window manager
export _JAVA_AWT_WM_NONREPARENTING=1

# https://github.com/sindresorhus/pure
# to remove awful newline, remove print in pure.zsh/prompt_pure_preprompt_render()
fpath+=$HOME/.local/share/zsh/pure
autoload -U promptinit; promptinit
prompt pure

source /usr/share/fzf/key-bindings.zsh

# https://github.com/zsh-users/zsh-autosuggestions
source $HOME/.local/share/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# https://github.com/zsh-users/zsh-syntax-highlighting
source $HOME/.local/share/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
