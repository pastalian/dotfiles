# ~/.bashrc

[[ $- != *i* ]] && return

for p in ~/.cargo/bin ~/.local/bin ~/bin; do
    [[ -d $p/. ]] || continue
    [[ :$PATH: = *":$p:"* ]] || PATH=$p:$PATH
done

PS1='\w \$ '
PS1='\[\e[1;32m\]'${PS1}'\[\e[0m\]'

HISTFILE="$HOME/.cache/bash_history"
HISTCONTROL='ignoreboth:erasedups'

bind '"\C-p": history-search-backward'
bind '"\C-n": history-search-forward'
bind '"\e[Z": menu-complete'

alias l='ls --color=auto' ll='l -lh' la='ll -A'
alias rm='rm -iv' cp='cp -iv'
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias vim=nvim

export BROWSER=firefox
export EDITOR=nvim
export LESS='-R'
export LESSHISTFILE=-

export MOZC_CONFIGURATION_DIRECTORY=$HOME/.config/mozc
export LIBVIRT_DEFAULT_URI=qemu:///system
export _JAVA_AWT_WM_NONREPARENTING=1

export NNN_OPTS=ae
export NNN_BMS="d:$HOME/Documents;D:$HOME/Downloads;m:/media"
export NNN_ARCHIVE="\\.(7z|bz2|gz|rar|tar|xz|zip|zst)$"
export NNN_PLUG='i:imgview;p:preview-tui'
export NNN_SEL=/tmp/nnn-sel
