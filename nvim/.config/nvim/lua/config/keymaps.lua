local map = vim.keymap.set

-- terminal
map("n", "<c-`>", function()
  Snacks.terminal(nil, { cwd = LazyVim.root() })
end, { desc = "Terminal (Root Dir)" })
map("t", "<C-`>", "<cmd>close<cr>", { desc = "Hide Terminal" })

-- todo
vim.keymap.del("n", "<leader>n")
map("n", "<leader>nt", function()
  vim.cmd("silent grep '\\- \\[ ]' | copen")
end, { desc = "List TODO" })

-- diagnostic
map("n", "ge", vim.diagnostic.open_float, { desc = "Show diagnostic" })
