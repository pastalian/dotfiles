return {
  {
    "sindrets/diffview.nvim",
    cmd = { "DiffviewFileHistory", "DiffviewOpen" },
    config = true,
  },
  {
    "NeogitOrg/neogit",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      require("neogit").setup({
        integrations = { diffview = true },
      })
    end,
    keys = {
      { "<leader>gs", "<cmd>Neogit<cr>", desc = "Git Status" },
    },
  },
  {
    "epwalsh/obsidian.nvim",
    version = "*",
    event = { "BufReadPre " .. vim.fn.expand("~") .. "/Documents/notes/index.md" },
    dependencies = {
      "nvim-lua/plenary.nvim",
      {
        "3rd/image.nvim",
        cond = function()
          return vim.env.TMUX == nil
        end,
        lazy = true,
        build = false,
        opts = {
          processor = "magick_cli",
          integrations = {
            markdown = {
              only_render_image_at_cursor = true,
              resolve_image_path = function(document_path, image_path, fallback)
                local client = require("obsidian").get_client()
                local path = client:vault_root().filename .. "/assets/" .. image_path
                if vim.fn.filereadable(path) == 1 then
                  return path
                end
                return fallback(document_path, image_path)
              end,
            },
          },
        },
      },
    },
    opts = {
      workspaces = { { name = "notes", path = "~/Documents/notes" } },
      notes_subdir = "b",
      daily_notes = { folder = "journal", template = "daily.md" },
      new_notes_location = "notes_subdir",
      note_path_func = function(spec)
        return (spec.dir / tostring(spec.title)):with_suffix(".md")
      end,
      wiki_link_func = "use_alias_only",
      disable_frontmatter = true,
      templates = { folder = "templates" },
      ui = { enable = false },
      attachments = {
        img_folder = "assets",
        img_text_func = function(client, path)
          path = client:vault_relative_path(path) or path
          return string.format("![[%s]]", path.name)
        end,
      },
    },
    keys = {
      { "<leader>nl", "<cmd>ObsidianLinkNew<cr>", mode = "v", desc = "Create link" },
      { "<leader>nn", "<cmd>ObsidianNew<cr>", desc = "New note" },
      { "<leader>nN", "<cmd>ObsidianNewFromTemplate<cr>", desc = "New note (template)" },
      { "<leader>nr", "<cmd>ObsidianRename<cr>", desc = "Rename note" },
      { "<leader>nd", "<cmd>ObsidianToday<cr>", desc = "Today's note" },
    },
  },
}
