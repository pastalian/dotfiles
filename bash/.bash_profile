# ~/.bash_profile

[[ -f ~/.bashrc ]] && . ~/.bashrc

# auto start desktop environment
[ "$(tty)" = "/dev/tty1" ] && sway
