return {
  {
    "catppuccin/nvim",
    config = function()
      require("catppuccin").setup({
        flavour = "mocha",
        integrations = {
          alpha = false,
          blink_cmp = true,
          cmp = false,
          dashboard = false,
          dropbar = { enabled = false },
          flash = false,
          indent_blankline = { enabled = false },
          mini = false,
          neogit = true,
          rainbow_delimiters = false,
          telescope = { enabled = false },
        },
      })
      vim.cmd.colorscheme("catppuccin")
    end,
  },
  {
    "saghen/blink.cmp",
    version = "*",
    event = "InsertEnter",
    opts = {
      keymap = { preset = "super-tab" },
      completion = {
        documentation = {
          auto_show = true,
          auto_show_delay_ms = 500,
        },
      },
      sources = { default = { "lsp", "path", "snippets", "buffer" } },
    },
  },
  {
    "sindrets/diffview.nvim",
    cmd = { "DiffviewFileHistory", "DiffviewOpen" },
    config = true,
  },
  {
    "numToStr/FTerm.nvim",
    lazy = true,
    config = function()
      require("FTerm").setup({
        cmd = "/bin/fish",
        dimensions = { height = 0.9, width = 1 },
      })
    end,
  },
  {
    "ibhagwan/fzf-lua",
    cmd = "FzfLua",
    config = function()
      require("fzf-lua").setup({
        keymap = {
          fzf = {
            ["ctrl-q"] = "select-all+accept",
          },
        },
        files = {
          git_icons = false,
          fd_opts = "--color=never --type f --hidden --exclude .git --strip-cwd-prefix",
        },
        grep = {
          git_icons = false,
          rg_opts = "--column --line-number --no-heading --color=always --smart-case --max-columns=512 --hidden --trim --glob !.git",
          rg_glob_fn = function(query, opts)
            local regex, flags = query:match("^(.-)%s%-%-(.*)$")
            return (regex or query), flags
          end,
        },
        git = {
          commits = {
            actions = {
              ["ctrl-y"] = function(selected, _)
                local commit_hash = selected[1]:match("[^ ]+")
                vim.fn.setreg([[+]], commit_hash)
              end,
            },
          },
          bcommits = {
            actions = {
              ["ctrl-y"] = function(selected, _)
                local commit_hash = selected[1]:match("[^ ]+")
                vim.fn.setreg([[+]], commit_hash)
              end,
            },
          },
        },
        previewers = {
          builtin = {
            extensions = {
              ["jpg"] = { "ueberzug" },
              ["png"] = { "ueberzug" },
            },
            ueberzug_scaler = "fit_contain",
          },
        },
      })
    end,
    keys = {
      { "<leader>ff", "<cmd>FzfLua files<cr>", desc = "Files" },
      { "<leader>fa", "<cmd>FzfLua live_grep<cr>", desc = "Grep" },
      { "<leader>fb", "<cmd>FzfLua buffers<cr>", desc = "Buffers" },
      { "<leader>,", "<cmd>FzfLua buffers<cr>", desc = "Buffers" },
      { "<leader>fc", "<cmd>FzfLua commands<cr>", desc = "Commands" },
      { "<leader>fh", "<cmd>FzfLua oldfiles<cr>", desc = "Recent" },
      { "<leader>fq", "<cmd>FzfLua quickfix<cr>", desc = "Quickfix" },
      { "<leader>fk", "<cmd>FzfLua git_bcommits<cr>", desc = "Commits (buffer)" },
      { "<leader>fl", "<cmd>FzfLua git_commits<cr>", desc = "Commits" },
      { "<leader>cs", "<cmd>FzfLua lsp_document_symbols<cr>", desc = "Symbols" },
      { "gr", "<cmd>FzfLua lsp_references ignore_current_line=true jump1=false<cr>", desc = "References" },
    },
  },
  {
    "lewis6991/gitsigns.nvim",
    config = function()
      require("gitsigns").setup({
        on_attach = function(bufnr)
          local gs = package.loaded.gitsigns

          local function map(mode, l, r, opts)
            opts = opts or {}
            opts.buffer = bufnr
            vim.keymap.set(mode, l, r, opts)
          end

          map("n", "]c", function()
            if vim.wo.diff then
              return "]c"
            end
            vim.schedule(function()
              gs.next_hunk()
            end)
            return "<Ignore>"
          end, { expr = true })

          map("n", "[c", function()
            if vim.wo.diff then
              return "[c"
            end
            vim.schedule(function()
              gs.prev_hunk()
            end)
            return "<Ignore>"
          end, { expr = true })

          map({ "n", "v" }, "<leader>hs", ":Gitsigns stage_hunk<CR>")
          map({ "n", "v" }, "<leader>hr", ":Gitsigns reset_hunk<CR>")
          map("n", "<leader>hS", gs.stage_buffer)
          map("n", "<leader>hu", gs.undo_stage_hunk)
          map("n", "<leader>hR", gs.reset_buffer)
          map("n", "<leader>hp", gs.preview_hunk)
          map("n", "<leader>hb", function()
            gs.blame_line({ full = true })
          end)
        end,
      })
    end,
  },
  {
    "williamboman/mason.nvim",
    dependencies = {
      "williamboman/mason-lspconfig.nvim",
      "neovim/nvim-lspconfig",
    },
    config = function()
      require("mason").setup()
      local mason_lspconfig = require("mason-lspconfig")
      local lspconfig = require("lspconfig")
      mason_lspconfig.setup({
        handlers = {
          function(server_name)
            lspconfig[server_name].setup({
              on_attach = function(client, bufnr)
                if client.server_capabilities.documentHighlightProvider then
                  vim.api.nvim_create_augroup("lsp_document_highlight", { clear = false })
                  vim.api.nvim_clear_autocmds({ buffer = bufnr, group = "lsp_document_highlight" })
                  vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
                    group = "lsp_document_highlight",
                    buffer = bufnr,
                    callback = vim.lsp.buf.document_highlight,
                  })
                  vim.api.nvim_create_autocmd("CursorMoved", {
                    group = "lsp_document_highlight",
                    buffer = bufnr,
                    callback = vim.lsp.buf.clear_references,
                  })
                end
              end,
            })
          end,
        },
      })
    end,
  },
  {
    "echasnovski/mini.nvim",
    version = "*",
    config = function()
      require("mini.bufremove").setup()
    end,
  },
  {
    "NeogitOrg/neogit",
    dependencies = { "nvim-lua/plenary.nvim" },
    cmd = "Neogit",
    config = function()
      require("neogit").setup({
        integrations = { diffview = true },
      })
    end,
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "MunifTanjim/nui.nvim",
    },
    cmd = "Neotree",
    opts = {
      filesystem = {
        follow_current_file = { enabled = true },
      },
      source_selector = { winbar = true },
      window = {
        mappings = {
          ["Y"] = {
            function(state)
              local node = state.tree:get_node()
              local path = node:get_id()
              vim.fn.setreg("+", path, "c")
            end,
            desc = "copy_path_to_clipboard",
          },
        },
      },
    },
    keys = {
      { "<Leader>e", "<cmd>Neotree toggle=true<cr>", desc = "Explorer" },
      { "<Leader>be", "<cmd>Neotree source=buffers toggle=true<cr>", desc = "Buffer Explorer" },
      { "<Leader>ge", "<cmd>Neotree source=git_status toggle=true<cr>", desc = "Git Explorer" },
    },
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      vim.env.EXTENSION_WIKI_LINK = 1
      local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
      parser_config.markdown.install_info.requires_generate_from_grammar = true
      parser_config.markdown_inline.install_info.requires_generate_from_grammar = true

      require("nvim-treesitter.configs").setup({
        ensure_installed = {
          "gitcommit",
          "markdown",
          "markdown_inline",
        },
        highlight = { enable = true },
      })
    end,
  },
  {
    "epwalsh/obsidian.nvim",
    version = "*",
    event = {
      "BufReadPre " .. vim.fn.expand("~") .. "/Documents/notes/index.md",
    },
    dependencies = {
      "nvim-lua/plenary.nvim",
      {
        "3rd/image.nvim",
        cond = function()
          return vim.env.TMUX == nil
        end,
        lazy = true,
        build = false,
        opts = {
          processor = "magick_cli",
          integrations = {
            markdown = {
              resolve_image_path = function(document_path, image_path, fallback)
                local client = require("obsidian").get_client()
                local path = client:vault_root().filename .. "/assets/" .. image_path
                if vim.fn.filereadable(path) == 1 then
                  return path
                end
                return fallback(document_path, image_path)
              end,
            },
          },
        },
      },
    },
    opts = {
      workspaces = { { name = "notes", path = "~/Documents/notes" } },
      notes_subdir = "b",
      daily_notes = { folder = "journal", template = "daily.md" },
      new_notes_location = "notes_subdir",
      note_path_func = function(spec)
        return (spec.dir / tostring(spec.title)):with_suffix(".md")
      end,
      wiki_link_func = "use_alias_only",
      disable_frontmatter = true,
      templates = { folder = "templates" },
      attachments = {
        img_folder = "assets",
        img_text_func = function(client, path)
          path = client:vault_relative_path(path) or path
          return string.format("![[%s]]", path.name)
        end,
      },
    },
    keys = {
      { "<leader>nl", "<cmd>ObsidianLinkNew<cr>", mode = "v", desc = "Create link" },
      { "<leader>nn", "<cmd>ObsidianNew<cr>", desc = "New note" },
      { "<leader>nN", "<cmd>ObsidianNewFromTemplate<cr>", desc = "New note (template)" },
      { "<leader>nr", "<cmd>ObsidianRename<cr>", desc = "Rename note" },
      { "<leader>nd", "<cmd>ObsidianToday<cr>", desc = "Today's note" },
    },
  },
}
