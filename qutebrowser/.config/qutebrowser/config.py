config.load_autoconfig()

c.auto_save.session = True

config.bind('<Ctrl-i>', 'spawn --userscript qute-keepass -p ~/Documents/data/pw.kdbx', mode='insert')
config.bind(',M', 'spawn --detach mpv {url}')
config.bind(',m', 'hint links spawn --detach mpv {hint-url}')
config.bind(';p', 'hint links run open -p {hint-url}')
config.bind('B', 'set-cmd-text -s :bookmark-load -t')
config.bind('b', 'set-cmd-text -s :bookmark-load')
config.bind('gB', 'set-cmd-text -s :quickmark-load -t')
config.bind('gb', 'set-cmd-text -s :quickmark-load')
config.bind('wB', 'set-cmd-text -s :quickmark-load -w')
config.bind('wb', 'set-cmd-text -s :bookmark-load -w')

c.colors.webpage.preferred_color_scheme = 'dark'
c.completion.open_categories = ['history']
c.content.autoplay = False
c.content.blocking.adblock.lists = [
    'https://easylist.to/easylist/easylist.txt',
    'https://easylist.to/easylist/easyprivacy.txt',
    'https://secure.fanboy.co.nz/fanboy-annoyance.txt',
    'https://github.com/uBlockOrigin/uAssets/raw/master/filters/annoyances.txt',
    'https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters.txt',
    'https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2020.txt',
    'https://github.com/uBlockOrigin/uAssets/raw/master/filters/privacy.txt',
    'https://github.com/uBlockOrigin/uAssets/raw/master/filters/unbreak.txt'
]
c.content.cookies.accept = 'no-3rdparty'
c.content.headers.accept_language = 'en-US,en;q=0.9,ja;q=0.8'
c.content.notifications.enabled = False
c.content.pdfjs = True
c.content.register_protocol_handler = False
c.downloads.location.suggestion = 'filename'
c.downloads.position = 'bottom'
c.downloads.remove_finished = 0
c.editor.command = ['st', '-e', 'nvim', '-f', '{file}', '-c', 'normal {line}G{column0}']
c.fileselect.folder.command = ['st', '-c', 'picker', '-e', 'nnn', '-p{}']
c.fileselect.handler = 'external'
c.fileselect.multiple_files.command = ['st', '-c', 'picker', '-e', 'nnn', '-p{}']
c.fileselect.single_file.command = ['st', '-c', 'picker', '-e', 'nnn', '-p{}']
c.fonts.default_size = '8pt'
c.tabs.mousewheel_switching = False
c.tabs.show = 'multiple'
c.tabs.title.format = '{index}: {current_title}'
c.url.default_page = 'https://pastalian.gitlab.io/startpage/'
c.url.start_pages = 'https://pastalian.gitlab.io/startpage/'

c.url.searchengines['DEFAULT'] = 'https://searx.tiekoetter.com/search?q={}&language=all'
c.url.searchengines['d'] = 'https://duckduckgo.com/?q={}'
c.url.searchengines['lkl'] = 'https://docs.us.sios.com/spslinux/9.7.0/ja/search?q={}'
c.url.searchengines['lkw'] = 'https://docs.us.sios.com/sps/8.9.1/ja/search?q={}'
c.url.searchengines['lku'] = 'https://lkdkuserportal.sios.jp/hc/ja/search?3&query={}'
