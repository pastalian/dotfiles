local map = vim.keymap.set
local opts = {}

vim.g.mapleader = [[ ]]

map("n", "<Esc><Esc>", "<Cmd>nohlsearch<CR>", opts)

map("n", "<C-x><C-s>", "<Cmd>w<CR>", opts)
map("i", "<C-x><C-s>", "<Cmd>w<CR>", opts)

map("n", "<C-j>", "<C-w>j", opts)
map("n", "<C-k>", "<C-w>k", opts)
map("n", "<C-l>", "<C-w>l", opts)
map("n", "<C-h>", "<C-w>h", opts)

map("t", "<C-j>", "<C-\\><C-n><C-w>j", { noremap = true })
map("t", "<C-k>", "<C-\\><C-n><C-w>k", { noremap = true })
map("t", "<C-l>", "<C-\\><C-n><C-w>l", { noremap = true })
map("t", "<C-h>", "<C-\\><C-n><C-w>h", { noremap = true })

map("n", "L", "<Cmd>bnext<CR>", opts)
map("n", "H", "<Cmd>bprevious<CR>", opts)
map("n", "<Leader>bd", "<Cmd>lua MiniBufremove.delete()<CR>", opts)
map("n", "<Leader>bD", "<Cmd>lua MiniBufremove.delete(0, true)<CR>", opts)
map("n", "<Leader>wq", "<Cmd>close<CR>", opts)

map("n", "<C-`>", '<Cmd>lua require("FTerm").toggle()<CR>', opts)
map("t", "<C-`>", '<Cmd>lua require("FTerm").toggle()<CR>', opts)

map("n", "<Leader>s", '<Cmd>lua require("neogit").open()<CR>', opts)

map("n", "ge", vim.diagnostic.open_float, opts)
map("n", "[d", vim.diagnostic.goto_prev, opts)
map("n", "]d", vim.diagnostic.goto_next, opts)

map("n", "<leader>nt", function() vim.cmd("silent grep '\\- \\[ ]' | copen") end)

vim.api.nvim_create_autocmd("LspAttach", {
  group = vim.api.nvim_create_augroup("UserLspConfig", {}),
  callback = function(ev)
    local bufopts = { buffer = ev.buf, noremap = true, silent = true }
    map("n", "gD", vim.lsp.buf.declaration, bufopts)
    map("n", "gd", vim.lsp.buf.definition, bufopts)
    map("n", "gh", vim.lsp.buf.hover, bufopts)
    map("n", "gi", vim.lsp.buf.implementation, bufopts)
    map("n", "gy", vim.lsp.buf.type_definition, bufopts)
    map("n", "gk", vim.lsp.buf.signature_help, bufopts)
    map("i", "<C-k>", vim.lsp.buf.signature_help, bufopts)
    map("n", "<Leader>ca", vim.lsp.buf.code_action, bufopts)
    map("n", "<Leader>cf", function()
      vim.lsp.buf.format({ async = true })
    end, bufopts)
    map("n", "<Leader>cr", vim.lsp.buf.rename, bufopts)
  end,
})
