alias la 'ls -lAh'
alias rm 'rm -iv'
alias cp 'cp -iv'
alias mv 'mv -iv'
alias diff 'diff --color=auto'
alias vim 'nvim'
alias ssh 'TERM=xterm-256color /usr/bin/ssh'

set -U fish_greeting
set -x GPG_TTY (tty)

starship init fish | source
