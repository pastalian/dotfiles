# Dotfiles
This is my dotfiles repository.

## Usage
You can use [GNU Stow](https://www.gnu.org/software/stow/) to link these dotfiles.

Link the dotfiles you need
```sh
stow bspwm sxhkd
```

> You need to remove your existing dotfiles to proceed linking.

Unlink (you can also manually remove symlinks)
```sh
stow -D bspwm sxhkd
```

## Config
Edit [.stowrc](.stowrc) to configure the stow option. See `man stow`.


## Software
Some of the dotfiles are not used (probably outdated). Here is a list of what I'm using.

| Category  | Application             |
|-----------|-------------------------|
| OS        | Gentoo                  |
| WM        | sway                    |
| Launcher  | bemenu                  |
| Shell     | fish                    |
| Terminal  | kitty                   |
| Browser   | Firefox                 |
| Editor    | Neovim                  |
| Email     | NeoMutt + mbsync        |
| File      | nnn                     |
| Image     | nsxiv                   |
| Music     | mpd                     |
| PDF       | zathura                 |
| Video     | mpv                     |

## Screenshots

### Dirty
![dirty](https://i.imgur.com/0AxNlPC.png)

### NeoMutt
![neomutt](https://i.imgur.com/AfHFlJC.png)
