local o = vim.opt
local g = vim.g

o.clipboard = "unnamedplus"
o.completeopt = "menu,menuone,noselect"
o.conceallevel = 2
o.cursorline = true
o.dictionary = "/usr/share/dict/words"
o.expandtab = true
o.fileencodings = "ucs-bom,utf-8,sjis"
o.foldcolumn = "1"
o.foldlevel = 99
o.foldlevelstart = 99
o.history = 100
o.ignorecase = true
o.laststatus = 3
o.lazyredraw = true
o.mouse = "a"
o.number = true
o.scrolloff = 2
o.shell = "/bin/sh"
o.shiftwidth = 4
o.shortmess:append("Ic")
o.showcmd = false
-- o.showmode = false
o.signcolumn = "yes"
o.smartcase = true
o.smartindent = true
o.splitbelow = true
o.splitright = true
o.statusline = "%F%m%r%=%y %l/%L,%2v/%-2{virtcol('$') - 1}"
o.termguicolors = true
o.timeoutlen = 500
o.updatetime = 500
o.wildmode = "longest:full"

g.termdebug_config = { map_K = 0 }

if vim.env.TMUX then
  g.clipboard = {
    name = "tmux",
    copy = {
      ["+"] = "tmux load-buffer -w -",
      ["*"] = "tmux load-buffer -w -",
    },
    paste = {
      ["+"] = "tmux save-buffer -",
      ["*"] = "tmux save-buffer -",
    },
    cache_enabled = 1,
  }
elseif vim.env.WAYLAND_DISPLAY then
  g.clipboard = {
    name = "wl-copy",
    copy = {
      ["+"] = "wl-copy --foreground --type text/plain",
      ["*"] = "wl-copy --foreground --primary --type text/plain",
    },
    paste = {
      ["+"] = "wl-paste --no-newline",
      ["*"] = "wl-paste --no-newline --primary",
    },
    cache_enabled = 1,
  }
elseif vim.env.DISPLAY then
  g.clipboard = {
    name = "xsel",
    copy = {
      ["+"] = "xsel --nodetach -i -b",
      ["*"] = "xsel --nodetach -i -p",
    },
    paste = {
      ["+"] = "xsel -o -b",
      ["*"] = "xsel -o -p",
    },
    cache_enabled = 1,
  }
end
